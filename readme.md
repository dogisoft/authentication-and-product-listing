# Test task

I am using Laravel framework for the PHP part and displaying the table with dataTable library.

The page based on Twitter Bootstrap and it is fully responsive. Just like to table.

Users are able to register, login and recover forgot password.

After the login, they will get a page with a list of products. The list is displayed as a table.

As it has been requested, the table is sortable, searchable and there is an option to export search results in PDF.

All the migration files are included.

Tamas Dogi 