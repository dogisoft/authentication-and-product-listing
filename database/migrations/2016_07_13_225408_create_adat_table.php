<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prodnum');
            $table->string('prodname');
            $table->string('netprice');
            $table->string('vat');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adat');
    }
}
