@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">List of items</div>

                <div class="panel-body">

                <table id="test" class="display responsive nowrap" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Item number</th>
                            <th>Product name</th>
                            <th>Net price</th>
                            <th>VAT</th>  
                            <th>Created</th>  
                            <th>Updated</th>  
                        </tr>
                    </thead>
                   

                </table>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection

